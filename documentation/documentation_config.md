---
project: Fortran Dictionary
project_gitlab: https://gitlab.com/fortran_utilities/fortran_dictionary
project_download: https://gitlab.com/fortran_utilities/fortran_dictionary/-/archive/main/fortran_dictionary-main.tar.gz
summary:
	A small toy project to implement a crude python
	dictionary like type in fortran.  It is not intended to be efficient,
	flexible or complete.
author: D Dickinson
author_description:
	Computational physicist based at the University of York
parallel: 16
src_dir: ../src
css: user.css
output_dir: ./html
graph_dir: ./graphs
page_dir: ./pages
media_dir: ./media
fpp_extensions: F90
predocmark: >
predocmark_alt: !
docmark: <
docmark_alt: #
display: public
display: protected
display: private
source: true
print_creation_date: true
search: false
graph: false
coloured_edges: true
---

{!../README.md!}
