# Adds target for pfunit tests in test_source
# name fd_tests_${test_name} and adds to
# list of know tests cases to give to ctest.
function(fd_add_test test_source test_name)
  add_pfunit_test(fd_tests_${test_name}
    "${test_source}" "" "")
  target_link_libraries(fd_tests_${test_name} fortran_dictionary)
  list(APPEND FD_CTEST_CASES fd_tests_${test_name})
  set(FD_CTEST_CASES ${FD_CTEST_CASES} PARENT_SCOPE)
endfunction()

# Helper function to easily add multiple separate
# tests provided they exist at tests/test_${name}.pf
# and we're happy to identify them as fd_tests_${name}
function(fd_add_tests)
  cmake_parse_arguments(
	FD_ADD "" "" "TEST_NAMES" ${ARGN}
    )
  foreach(name ${FD_ADD_TEST_NAMES})
    fd_add_test("test_${name}.pf" ${name})
  endforeach()
  set(FD_CTEST_CASES ${FD_CTEST_CASES} PARENT_SCOPE)
endfunction()
