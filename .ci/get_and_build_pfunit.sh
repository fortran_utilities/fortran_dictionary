#/bin/bash
set -e

if [ -d ${PKG_CACHE_DIR}/pfunit ]
then
    echo "${PKG_CACHE_DIR}/pfunit already exists, skipping download and build."
else
    mkdir -p ${PKG_CACHE_DIR}
    git clone https://github.com/Goddard-Fortran-Ecosystem/pFUnit
    cmake pFUnit -B pfunit_build -DSKIP_MPI=YES -DSKIP_OPENMP=YES -DCMAKE_INSTALL_PREFIX=${PKG_CACHE_DIR}/pfunit
    cmake --build pfunit_build --target install
fi
